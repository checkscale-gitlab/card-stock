FROM python:3.8.2-alpine

COPY . /app

WORKDIR /app

RUN apk add --no-cache  bash g++ gcc libffi-dev libressl-dev libxslt-dev build-base libmemcached-dev
RUN pip install -r requirements.txt
RUN chmod +x scripts/*

CMD ["python", "run.py"]

EXPOSE 9090