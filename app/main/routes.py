import json

from requests.exceptions import HTTPError
from flask import Blueprint, render_template, flash, redirect, url_for, session, request, abort
from flask_login import login_required, login_user, current_user, logout_user
from service import card_service
from app import db, login_manager, auth
from app.forms import AddCard, AddCategory
from app.models import CategoryModel, CardModel, UserModel
from requests_oauthlib import OAuth2Session
from . import logger

main_blueprint = Blueprint('main_blueprint', __name__,
                           template_folder='templates', )


@main_blueprint.route('/')
@login_required
def index():
    return render_template('index.html')


@main_blueprint.route('/tracked', methods=['GET'])
@login_required
def watching():
    cards = card_service.CardService().find_listings()
    return render_template('tracked.html', cards=cards, show_more=True)


@main_blueprint.route('/more/<path:card_key>', methods=['GET'])
def see_more(card_key):
    f_card_key = card_key.replace("_", " ")
    cards = card_service.CardService().find_listings_for_card(f_card_key)
    return render_template('tracked.html', cards=cards, show_more=False)


@main_blueprint.route('/add', methods=['GET', 'POST'])
@login_required
def add_to_watched():
    add_category_form = AddCategory()

    choices = CategoryModel.query.with_entities(CategoryModel.ebay_category_id, CategoryModel.ebay_category_name)
    add_card_form = AddCard()
    add_card_form.category_id.choices = choices

    errors = None

    if add_category_form.submit_category.data and add_category_form.validate_on_submit():
        errors = handle_category_form(add_category_form)

        if errors is None:
            return redirect(url_for('main_blueprint.add_to_watched'))
        else:
            logger.error("Form validation for adding category failed - {}".format(errors))

    if add_card_form.submit_card.data and add_card_form.validate_on_submit():
        errors = handle_card_form(add_card_form)

        if errors is None:
            return redirect(url_for('main_blueprint.add_to_watched'))
        else:
            logger.error("Form validation for adding card to track failed; errors={}".format(errors))

    return render_template('add.html', category_form=add_category_form, card_form=add_card_form, errors=errors)


@main_blueprint.route('/login')
def login():
    """
    Handle Login requests
    """

    if current_user.is_authenticated:
        return redirect(url_for('main_blueprint.index'))
    google = get_google_auth()
    auth_url, state = google.authorization_url(
        auth.auth_uri, access_type='offline')
    session['oauth_state'] = state

    return render_template('login.html', auth_url=auth_url)


@main_blueprint.route('/logout')
def logout():
    """
    Log out the user
    """
    logout_user()
    return redirect(url_for('main_blueprint.index'))


@login_manager.user_loader
def load_user(user_id):
    return UserModel.query.get(int(user_id))


@main_blueprint.route('/callback')
def callback():
    """
    Handle OAuth callbacks
    """
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('main_blueprint.index'))

    if 'error' in request.args:
        if request.args.get('error') == 'access_denied':
            return 'You denied access.'
        return 'Error encountered.'

    if 'code' not in request.args and 'state' not in request.args:
        return redirect(url_for('main_blueprint.login'))
    else:
        is_authenticated = can_authenticate_user(session)

        if is_authenticated:
            return redirect(url_for('main_blueprint.index'))

        return abort(403)


def can_authenticate_user(user_session):
    """
    Authenticate the user
    """
    is_authenticated = False

    oauth_session = create_oauth2_session(user_session['oauth_state'])
    token = get_oauth_token(oauth_session)

    google_oauth = create_oauth2_session_with_token(token=token)
    resp = google_oauth.get(auth.user_info)

    if resp.status_code == 200:
        is_authenticated = True
        log_user_in(resp, token)

    return is_authenticated


def get_google_auth():
    oauth = OAuth2Session(
        auth.client_id,
        redirect_uri=auth.redirect_uri,
        scope=auth.scope)
    return oauth


def create_oauth2_session(state):
    """
    Creates an OAuth2 session
    """
    return OAuth2Session(auth.client_id, state=state, redirect_uri=auth.redirect_uri)


def get_oauth_token(oauth_session):
    """
    Get Oauth token
    """
    token = None
    try:
        token = oauth_session.fetch_token(auth.token_uri, client_secret=auth.client_secret,
                                          authorization_response=request.url)
        return token
    except HTTPError:
        logger.error("Not able to retrieve Oauth token! response={}", HTTPError.response)
        return token


def create_oauth2_session_with_token(token):
    """
    Authenticate the Oauth token
    """

    return OAuth2Session(auth.client_id, token=token)


def log_user_in(resp, token):
    """
    Logs in the user and updates the database
    """
    user_data = resp.json()
    email = user_data['email']
    user = UserModel.query.filter_by(email=email).first()

    if user is None:
        user = UserModel()
        user.email = email

    user.name = user_data['name']
    user.tokens = json.dumps(token)
    user.avatar = user_data['picture']

    add_item_to_db(user)
    login_user(user)


def handle_category_form(add_category_form):
    """
    Handle POST for category form
    """
    errors = None
    new_id = add_category_form.id.data
    category_name = add_category_form.category_name.data

    existing_category = CategoryModel.query.filter_by(ebay_category_id=new_id).first()

    if existing_category:
        errors = "Category exists in database"
    else:
        new_category = CategoryModel(ebay_category_id=new_id, ebay_category_name=category_name)
        add_item_to_db(new_category)

        flash('Added new category')
        logger.info("Added category to database; category={}".format(category_name))

    return errors


def handle_card_form(add_card_form):
    """
    Handle POST for card form
    """
    errors = None
    category_id = add_card_form.category_id.data
    card = add_card_form.card_name.data

    existing_card = db.session.query(CardModel).filter_by(name=card).first()

    if existing_card:
        errors = "Card exists in database"
    else:
        new_card = CardModel(category_id=category_id, name=card)
        add_item_to_db(new_card)

        flash('Card - {} - added to database'.format(card))
        logger.info("Added category to database; card={}".format(card))

    return errors


# noinspection PyMethodMayBeStatic
def add_item_to_db(item):
    """
    Adds the item to the database
    """

    db.session.add(item)
    db.session.commit()
