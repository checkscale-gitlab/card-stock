from . import db
from flask_login import UserMixin


class UserModel(db.Model, UserMixin):
    """
    Model for User
    """
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    name = db.Column(db.String(100), nullable=True)
    avatar = db.Column(db.String(200))
    tokens = db.Column(db.Text)
    created_at = db.Column(db.DateTime, server_default=db.func.now())


class CategoryModel(db.Model):
    """
    Model for card category
    """

    __tablename__ = "categories"

    id = db.Column(db.Integer, primary_key=True)
    ebay_category_id = db.Column(db.Integer,
                                 unique=True,
                                 index=True,
                                 nullable=False)
    ebay_category_name = db.Column(db.String(256),
                                   unique=True,
                                   nullable=False,
                                   index=False,)

    created = db.Column(db.DateTime,
                        index=False,
                        unique=False,
                        nullable=False,
                        server_default=db.func.now(),)

    card = db.relationship("CardModel", backref="CategoryModel")

    def __repr__(self):
        return "<Category {}".format(self.ebay_category_id)


class CardModel(db.Model):
    """
    Model for Cards
    """

    __tablename__ = "cards"

    id = db.Column(db.Integer, primary_key=True)

    category_id = db.Column(db.Integer,
                            db.ForeignKey('categories.ebay_category_id'),)

    name = db.Column(db.String(256),
                     unique=True,
                     nullable=False,
                     index=True,)
    owned = db.Column(db.Boolean,
                      index=False,
                      unique=False,
                      nullable=False,
                      default=False,)
    created = db.Column(db.DateTime,
                        index=False,
                        unique=False,
                        nullable=False,
                        server_default=db.func.now(),)

    updated = db.Column(db.DateTime,
                        index=False,
                        unique=False,
                        nullable=False,
                        server_default=db.func.now(),
                        server_onupdate=db.func.now(),)

    def __repr__(self):
        return '<name={}; category={}>'.format(self.name, self.category_id)
