from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField, IntegerField
from wtforms.validators import DataRequired


class AddCategory(FlaskForm):
    """
    Python web form for adding an Ebay category
    """

    id = IntegerField(u'Category ID', validators=[DataRequired()])
    category_name = StringField(u'Category Name', validators=[DataRequired()])
    submit_category = SubmitField('Add category')


class AddCard(FlaskForm):
    """
    Python web form for adding a card to watch

    Supported eBay category ids:
        216 - Ice Hockey
        213 - Baseball
    """

    category_id = SelectField(u'Category ID', choices=[], validators=[DataRequired()], coerce=int)
    card_name = StringField(u'Card Name', validators=[DataRequired()])
    submit_card = SubmitField('Add card')
